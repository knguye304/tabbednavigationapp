﻿using System.Diagnostics;
using Xamarin.Forms;


namespace TabbedNavigationApp
{ 
	public partial class TabbingPage : TabbedPage
	{
		public TabbingPage()
		{
			InitializeComponent ();
		}
        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"****{this.GetType().Name}.{nameof(OnDisappearing)}");
        }

    }
}